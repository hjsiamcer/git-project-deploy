## git-project-deploy
这是一个基于git+码云的自动部署项目，本版本提供多项目部署支持

## 使用

**1、下载**
``` shell
git clone https://gitee.com/hjsiamcer/git-project-deploy.git
```
**2、修改配置文件**

vim conf.json
``` json
{
  "11111111" :{
     "project_url":"https://gitee.com/hjsiamcer/sucheng.git",
     "deploy_dir":"/phpstudy/webroot/suchengkeji"
   }
}
```
`conf.json`中，11111111是webhook回调地址的验证密码，project_url是仓库的https路径，deploy_dir是项目部署目录

**3、编写webhook回调接口**

``` php
<?php
 $raw_post_data = file_get_contents('php://input', 'r');

        //参数文件
        $file =  '/home/hjs/git-project-deploy/pull_param.json';
        file_put_contents($file,$raw_post_data);

        $postData = json_decode($raw_post_data,true);

        //调用部署脚本
        $cmd  = '/home/hjs/git-project-deploy/auto.py /home/hjs/git-project-deploy ' . ' '.$postData['password'];
        echo  shell_exec($cmd);
		
?>
```


如果有什么问题请发邮件给我 :) 17389773414@163.com



