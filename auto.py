#!/bin/env python
#!coding=utf-8

# 自动部署入口
# 主要分为以下几个部分：
# 1、代码拉取
# 2、编译
# 3、更新数据表
# 4、日志
# 5、异常处理
import os 
import sys
import json



#部署口令
token = '11111111'



def main(projectPath,passwd):
    #当前目录
    os.chdir(projectPath)
   

    fb = open('pull_param.json','r')
    pull_param = fb.read()
 
    pull_param = json.loads(pull_param)

    if (pull_param['ref'] == u'refs/heads/master' and  pull_param['password'] == token):
         print '--------------------拉取代码--------------------------'
         cmd =  './pull_code.py '+ projectPath + ' ' + passwd
         os.system(cmd)
  
         print '--------------------编译------------------'
         # os.system('./compile.sh')


    else:
        print '认证失败'


if len(sys.argv) >= 3 :
    projectPath = sys.argv [1]
    passwd = sys.argv[2] 
    main(projectPath,passwd)

else:
    print '请传入部署脚本所在地址 和项目配置项索引'


