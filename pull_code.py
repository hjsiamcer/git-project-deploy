#!/bin/env python
#!coding=utf-8

import os
import json
import sys
# 代码拉取脚本
# 网站根目录地址
#如果项目目录存在，那就直接进去，然后pull
#如果目录不存在，那就直接git clone

class PullCode(object):

    def deploy(self,projectPath,passwd):
        pullParam = self.__getConf(projectPath)

        pullParam =  pullParam[passwd]

        projectUrl =  pullParam['project_url']
        deployDir =  pullParam['deploy_dir']
 

        if (os.path.exists(deployDir)):
            os.chdir(deployDir)    
            cmd = 'git pull origin master'
            os.system(cmd)   

        else:
            print '目录不存在,直接克隆master分支的指定版本'
            cmd = 'git clone '+ projectUrl + ' ' +  deployDir
            os.system(cmd)

    # 配置文件参数
    def __getConf(self,projectPath):
        fb = open(projectPath+'/conf.json','r')
        pull_param = fb.read()
        pull_param = json.loads(pull_param)
        return pull_param


if (len(sys.argv) >=3) :

    pullCode = PullCode()
    pullCode.deploy(sys.argv[1],sys.argv[2]) 
else:
    print '请传递脚本绝对路径目录和密码'
